package com.example.testweatherapplication.data

import androidx.lifecycle.LiveData
import com.example.testweatherapplication.App
import com.example.testweatherapplication.data.entity.City
import com.example.testweatherapplication.data.model.WeatherData
import com.example.testweatherapplication.room.AppDatabase
import com.example.testweatherapplication.net.ApiUtil
import com.example.testweatherapplication.util.Resource
import retrofit2.HttpException
import java.lang.Exception
import javax.inject.Inject

interface WeatherRepo {
    suspend fun getWeatherByCity(cityName: String): Resource<WeatherData>
    suspend fun addFavoriteCity(city: City)
    suspend fun removeFavoriteCity(city: City)
    fun getFavoriteCities(): LiveData<List<City>>
}

class WeatherRepoImpl : WeatherRepo {

    init {
        App.component()?.inject(this)
    }

    @Inject
    lateinit var apiUtil: ApiUtil

    @Inject
    lateinit var appDatabase: AppDatabase

    override suspend fun getWeatherByCity(cityName: String): Resource<WeatherData> {
        return try {
            val weatherData = apiUtil.getApiInterfaceKt()?.tasksByWarehouse(
                cityName,
                ApiUtil.Const.UNITS_CNT,
                ApiUtil.Const.UNITS_QUERY,
                ApiUtil.Const.API_KEY
            )
            Resource.success(weatherData)
        } catch (e: HttpException) {
            val errorMess = if (e.code() == 404){
                "City not found"
            } else {
                "Try again later"
            }
            Resource.error(errorMess, null)
        } catch (e: Exception) {
            Resource.error(e.message ?: "", null)
        }
    }

    override suspend fun addFavoriteCity(city: City) {
        appDatabase.getCityDao().insert(city)
    }

    override suspend fun removeFavoriteCity(city: City) {
        appDatabase.getCityDao().delete(city)
    }

    override fun getFavoriteCities() = appDatabase.getCityDao().getAllCities()
}