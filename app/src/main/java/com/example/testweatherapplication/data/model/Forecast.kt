package com.example.testweatherapplication.data.model

import com.google.gson.annotations.SerializedName

data class Forecast(

    @SerializedName("dt_txt")
    val text: String,

    @SerializedName("dt")
    val dateTimeStamp: Long,

    @SerializedName("main")
    val mainTemp: MainTemp,

    val wind: WindInfo,

    val weather: List<WeatherInfo>
) {}
