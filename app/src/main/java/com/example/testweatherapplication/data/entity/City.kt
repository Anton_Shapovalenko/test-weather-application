package com.example.testweatherapplication.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_cities")
data class City(
    @PrimaryKey
    var id: String = "",

    var name: String,

    var country: String
) {
    companion object {
        const val tableName = "tb_cities"
    }
}