package com.example.testweatherapplication.data.model

import com.google.gson.annotations.SerializedName

data class MainTemp(
    val temp: Double,
    val humidity: Int,
    @SerializedName("feels_like")
    val feelsTemp: Double,
)
