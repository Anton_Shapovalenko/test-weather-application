package com.example.testweatherapplication.data.model

data class WeatherInfo(val main:String, val description:String, val icon:String)
