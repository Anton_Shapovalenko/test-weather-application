package com.example.testweatherapplication.data.model

import com.example.testweatherapplication.data.entity.City
import com.google.gson.annotations.SerializedName

data class WeatherData(
    var city: City,
    var cod: String,

    @SerializedName("list")
    var listWeather: List<Forecast>
) {
}