package com.example.testweatherapplication.net

import com.example.testweatherapplication.data.model.WeatherData
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("forecast")
    suspend fun tasksByWarehouse(
        @Query("q") cityName: String, @Query("cnt") cnt: String, @Query("units") units: String, @Query("appid") appId: String
    ): WeatherData
}