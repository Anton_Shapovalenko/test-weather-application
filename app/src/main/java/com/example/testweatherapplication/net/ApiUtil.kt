package com.example.testweatherapplication.net

import android.content.Context
import android.util.Log
import com.example.testweatherapplication.net.ApiUtil.Const.BASE_URL
import com.example.testweatherapplication.net.ApiUtil.Const.CONNECT_TIMEOUT
import com.example.testweatherapplication.net.ApiUtil.Const.RW_TIMEOUT
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ApiUtil {
    object Const {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val API_KEY = "12b05dcb4ce39f86a8471a408f15a591"
        const val UNITS_QUERY = "metric"
        const val UNITS_CNT = "16"
        const val CONNECT_TIMEOUT = 15
        const val RW_TIMEOUT = 30
        fun getImageUrl(id: String) = "https://openweathermap.org/img/wn/$id@2x.png"
    }

    private var mApiInterfaceKt: ApiInterface? = null
    fun getApiInterfaceKt() = mApiInterfaceKt

    private var mContext: Context? = null


    @Inject
    constructor(context: Context) {
        mContext = context
        mApiInterfaceKt = getRetrofit().create(ApiInterface::class.java)
    }


    private fun getHttpClient(): OkHttpClient? {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
        builder.readTimeout(RW_TIMEOUT.toLong(), TimeUnit.SECONDS)
        builder.writeTimeout(RW_TIMEOUT.toLong(), TimeUnit.SECONDS)
        builder.addNetworkInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
            requestBuilder.method(original.method(), original.body())
            val request = requestBuilder.build()
            val response = chain.proceed(request)
            if (response.code() == 401) {
                Log.e(
                    "SESSION",
                    "ALARM 401|Session is empty:"
                )
            }
            response
        }
        return builder.build()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .client(getHttpClient())
            .build()
    }

    private fun getGson(): Gson? {
        return GsonBuilder()
            .setLenient()
            .create()
    }
}