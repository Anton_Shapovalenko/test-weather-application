package com.example.testweatherapplication.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.testweatherapplication.data.entity.City

@Database(entities = [City::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCityDao(): CitiesRoomDao
}