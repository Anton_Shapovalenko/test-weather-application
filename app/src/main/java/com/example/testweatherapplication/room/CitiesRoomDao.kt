package com.example.testweatherapplication.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.testweatherapplication.data.entity.City

@Dao
interface CitiesRoomDao {

    @Query("SELECT * FROM ${City.tableName}")
    fun getAllCities(): LiveData<List<City>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(city: City)

    @Delete()
    fun delete(city: City)
}