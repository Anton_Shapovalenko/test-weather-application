package com.example.testweatherapplication.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.*
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.testweatherapplication.R
import com.example.testweatherapplication.ui.favorite.FavoriteCitiesDialogFragment
import com.example.testweatherapplication.util.observeOnce
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private var mainViewModel: MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(findViewById(R.id.toolbar))
        val mAdapterRv = WeatherRecyclerViewAdapter()
        rv_weather?.run {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapterRv
        }
        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel?.run {
            //check favorite cities from Room for search this city when open app
            getAllCities().observeOnce(this@MainActivity) {
                if (it.isEmpty()) {
                    updateWeatherInfo("London")
                } else {
                    updateWeatherInfo(it[0].name)
                }
            }

            selectedCity.observe(this@MainActivity, {
                toolbar_layout.title = getString(R.string.weather_in, it.name)
            })
            listWeather.observe(this@MainActivity, {
                toolbar.collapseActionView()
                app_bar.setExpanded(true)
                mAdapterRv.updateList(it)
            })
            errorMessage.observe(this@MainActivity, {
                Snackbar.make(window.decorView.rootView, it, Snackbar.LENGTH_SHORT)
                    .show()
            })
        }
        fabFavorite.setOnClickListener {
            // add city to favorite Room
            mainViewModel?.addToFavoriteCity()
            Snackbar.make(window.decorView.rootView, getString(R.string.add_to_favorite), Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.queryHint = "Search city"

        menu.findItem(R.id.action_search)
            .setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                    app_bar.setExpanded(false)  //rollup toolbar
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                    return true
                }
            })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                mainViewModel?.updateWeatherInfo(query ?: "")
                return false
            }
        })

        menu.findItem(R.id.action_cities).setOnMenuItemClickListener {
            //open DialogFragment with favorite cities
            FavoriteCitiesDialogFragment().show(supportFragmentManager.beginTransaction(), "dialog")
            true
        }
        return true
    }
}