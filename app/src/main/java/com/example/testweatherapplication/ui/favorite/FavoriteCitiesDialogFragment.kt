package com.example.testweatherapplication.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.iterator
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testweatherapplication.R
import com.example.testweatherapplication.data.entity.City
import com.example.testweatherapplication.ui.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_dialog_favorit_cities.*

class FavoriteCitiesDialogFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog_favorit_cities, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = FavoriteCitiesAdapter()
        recyclerViewFavorite.layoutManager = LinearLayoutManager(context)
        recyclerViewFavorite.adapter = adapter
        recyclerViewFavorite.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        val mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        mainViewModel.getAllCities().observe(viewLifecycleOwner) { listCities ->
            if (listCities.isNullOrEmpty()) {
                recyclerViewFavorite.visibility = View.INVISIBLE
                tvEmptyList.visibility = View.VISIBLE
            } else {
                adapter.updateList(listCities)
            }
        }
        adapter.eventListener = object : FavoriteCitiesAdapter.OnClickEventRv {
            override fun openCity(city: City) {
                mainViewModel.updateWeatherInfo(city.name)
                dismiss()
            }

            override fun removeCity(city: City) {
                mainViewModel.removeFromFavorite(city)
            }
        }
    }
}