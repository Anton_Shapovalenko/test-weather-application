package com.example.testweatherapplication.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testweatherapplication.R
import com.example.testweatherapplication.data.model.Forecast
import com.example.testweatherapplication.net.ApiUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_weather_adapter.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class WeatherRecyclerViewAdapter() :
    RecyclerView.Adapter<WeatherRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDescription: TextView = itemView.tv_description
        val tvDate: TextView = itemView.tv_data
        val tvDegree: TextView = itemView.tv_degree
        val tvWindSpeed: TextView = itemView.tv_wind_speed
        val tvRainy: TextView = itemView.tv_rainy
        val imageWeather: ImageView = itemView.weather_icon
    }

    private val listForecast = ArrayList<Forecast>()

    fun updateList(list: List<Forecast>) {
        listForecast.clear()
        listForecast.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_weather_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            val forecast = listForecast[position]
            tvDescription.text = forecast.weather[0].description ?: ""
            tvRainy.text = tvRainy.resources.getString(R.string.rainy, forecast.mainTemp.humidity)
            tvWindSpeed.text =
                tvWindSpeed.resources.getString(R.string.wind_speed, forecast.wind.speed)
            tvDegree.text = "${forecast.mainTemp.temp.roundToInt()}"
            tvDate.text = "${getDate(forecast.dateTimeStamp)}"

            Glide.with(imageWeather.context)
                .load(ApiUtil.Const.getImageUrl(forecast.weather[0].icon)).into(imageWeather)
        }
    }

    override fun getItemCount() = listForecast.size

    companion object {
        private fun getDate(time_stamp_server: Long): String? {
            val mounth = SimpleDateFormat("MMM", Locale.US)
            val day = SimpleDateFormat("EEE", Locale.US)
            val formatter = SimpleDateFormat("dd - hh:mm aa", Locale.US)

            return "${day.format(time_stamp_server * 1000)},  ${mounth.format(time_stamp_server * 1000)} ${
                formatter.format(
                    time_stamp_server * 1000
                )
            }"
        }
    }
}