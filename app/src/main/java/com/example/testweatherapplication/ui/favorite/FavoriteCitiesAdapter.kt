package com.example.testweatherapplication.ui.favorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.testweatherapplication.R
import com.example.testweatherapplication.data.entity.City
import kotlinx.android.synthetic.main.item_city_adapter.view.*

class FavoriteCitiesAdapter() :
    RecyclerView.Adapter<FavoriteCitiesAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val parentLayout: ConstraintLayout = itemView.parentLayout
        val tvCityName: TextView = itemView.tv_city_name
        val btnRemove: ImageButton = itemView.btn_remove
    }

    private var listOfCity: ArrayList<City> = ArrayList<City>()
    var eventListener: OnClickEventRv? = null


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_city_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            tvCityName.text = listOfCity[position].name
            btnRemove.setOnClickListener {
                eventListener?.removeCity(listOfCity[position])
            }
            parentLayout.setOnClickListener {
                eventListener?.openCity(listOfCity[position])
            }
        }
    }


    fun updateList(list: List<City>) {
        listOfCity.clear()
        listOfCity.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount() = listOfCity.size

    interface OnClickEventRv {
        fun openCity(city: City)
        fun removeCity(city: City)
    }
}