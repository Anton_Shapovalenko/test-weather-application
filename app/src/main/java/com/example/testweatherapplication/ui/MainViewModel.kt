package com.example.testweatherapplication.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testweatherapplication.App
import com.example.testweatherapplication.data.entity.City
import com.example.testweatherapplication.data.model.Forecast
import com.example.testweatherapplication.data.WeatherRepo
import com.example.testweatherapplication.util.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel : ViewModel() {

    @Inject
    lateinit var weatherRepo: WeatherRepo

    val selectedCity = MutableLiveData<City>()
    val errorMessage = MutableLiveData<String>()
    val listWeather = MutableLiveData<List<Forecast>>()

    init {
        App.component()?.inject(this)
    }
    //Get weather by city
    fun updateWeatherInfo(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val resWeatherData = weatherRepo.getWeatherByCity(cityName)
            when (resWeatherData.status) {
                Status.SUCCESS -> {
                    selectedCity.postValue(resWeatherData.data?.city)
                    listWeather.postValue(resWeatherData.data?.listWeather)
                }
                Status.ERROR -> {
                    errorMessage.postValue(resWeatherData.message ?: "Unknown error")
                }
            }
        }
    }

    fun addToFavoriteCity() {
        viewModelScope.launch(Dispatchers.IO) {
            selectedCity.value?.let {
                weatherRepo.addFavoriteCity(it)
            }
        }
    }

    fun removeFromFavorite(city: City) {
        viewModelScope.launch(Dispatchers.IO) {
            weatherRepo.removeFavoriteCity(city)

        }
    }

    fun getAllCities(): LiveData<List<City>> = weatherRepo.getFavoriteCities()

}