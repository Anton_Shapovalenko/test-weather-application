package com.example.testweatherapplication

import android.app.Application
import com.example.testweatherapplication.dagger.AppComponent
import com.example.testweatherapplication.dagger.AppModule
import com.example.testweatherapplication.dagger.DaggerAppComponent

class App : Application() {

    companion object {

        private var appComponent: AppComponent? = null

        fun component(): AppComponent? {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()
        buildComponentGraph()
    }


    private fun buildComponentGraph() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}