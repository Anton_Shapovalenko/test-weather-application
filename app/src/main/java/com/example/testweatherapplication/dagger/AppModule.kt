package com.example.testweatherapplication.dagger

import android.content.Context
import androidx.room.Room
import com.example.testweatherapplication.data.WeatherRepo
import com.example.testweatherapplication.data.WeatherRepoImpl
import com.example.testweatherapplication.room.AppDatabase
import com.example.testweatherapplication.net.ApiUtil
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideApiUtil() = ApiUtil(context)

    @Provides
    @Singleton
    fun getRepo(): WeatherRepo = WeatherRepoImpl()

    @Provides
    @Singleton
    fun getDataBase(): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "database")
            .fallbackToDestructiveMigration()
            .build()
    }
}