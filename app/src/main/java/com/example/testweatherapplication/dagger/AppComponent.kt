package com.example.testweatherapplication.dagger

import com.example.testweatherapplication.App
import com.example.testweatherapplication.data.WeatherRepoImpl
import com.example.testweatherapplication.ui.MainViewModel
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent : AndroidInjector<App> {
    fun inject(viewModel: MainViewModel)
    fun inject(viewModel: WeatherRepoImpl)
}